import { ProductButton } from './components/ProductButton';
import { ProductList } from './components/ProductList';

function App() {
  return (
    <div className="container mt-3 mb-3">
      <div className="ml-1 mr-1 mb-1">
        <ProductButton />
      </div>
      <ProductList />
    </div>
  );
}

export default App;
