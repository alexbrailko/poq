import create from 'zustand';
import { ProductType, SelectedProductsType } from '../types/product';

interface ProductsState {
  products: ProductType[];
  loading: boolean;
  hasErrors: boolean;
  selectedProducts: SelectedProductsType[];
  fetchProducts: () => void;
  selectProduct: (id: SelectedProductsType) => void;
  unselectProduct: (id: SelectedProductsType) => void;
  deleteSelectedProducts: () => void;
}

export const useProductsStore = create<ProductsState>((set, get) => ({
  products: [],
  loading: false,
  hasErrors: false,
  selectedProducts: [],
  fetchProducts: async () => {
    set(() => ({ loading: true }));
    try {
      const response = await fetch(
        'https://run.mocky.io/v3/fca7ef93-8d86-4574-9a4a-3900d91a283e',
      );
      const products = await response.json();
      const filteredProducts = products

        .map((p: any) => {
          return {
            ...p,
            available: p.available === 'TRUE' ? true : false,
            lowOnStock: p.lowOnStock === 'TRUE' ? true : false,
          };
        })
        .filter((p: any) => p.available);

      set((state) => ({
        products: (state.products = filteredProducts),
        loading: false,
      }));
    } catch (err) {
      set(() => ({ hasErrors: true, loading: false }));
    }
  },
  selectProduct: (id) => {
    const foundId = get().selectedProducts.find(
      (productId) => productId === id,
    );

    if (!foundId) {
      set((state) => ({
        selectedProducts: [...state.selectedProducts, id],
      }));
    }
  },
  unselectProduct: (id) => {
    set((state) => ({
      selectedProducts: state.selectedProducts.filter((p) => p !== id),
    }));
  },
  deleteSelectedProducts: () => {
    const filteredProducts = get().products.filter(
      (p) => !get().selectedProducts.includes(p.productId),
    );
    set(() => ({
      products: filteredProducts,
      selectedProducts: [],
    }));
  },
}));
