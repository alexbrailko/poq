import { FC } from 'react';
import styled from 'styled-components';
import { useProductsStore } from '../store/products';

export const ProductButton: FC = () => {
  const selectedProducts = useProductsStore((state) => state.selectedProducts);
  const deleteSelectedProducts = useProductsStore(
    (state) => state.deleteSelectedProducts,
  );

  return (
    <Button
      disabled={selectedProducts.length ? false : true}
      onClick={deleteSelectedProducts}
    >
      Remove {selectedProducts.length} selected products
    </Button>
  );
};

const Button = styled.button`
  display: inline-block;
  border-radius: 5px;
  background-color: var(--purple);
  color: #fff;
  border: none;
  font-size: 16px;
  padding: 10px 15px;
  cursor: pointer;

  &:disabled {
    background-color: grey;
  }
`;
