import { FC, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { useProductsStore } from '../store/products';

import { ProductType } from '../types/product';

export const ProductCard: FC<ProductType> = ({
  productId,
  name,
  imageUrl,
  price,
  priceWas,
  promotionBadge,
  quantity,
  lowOnStock,
}) => {
  const selectProduct = useProductsStore((state) => state.selectProduct);
  const unselectProduct = useProductsStore((state) => state.unselectProduct);
  const [selected, setSelected] = useState(false);
  const prevSelected = useRef(false);

  useEffect(() => {
    if (selected) {
      selectProduct(productId);
      prevSelected.current = true;
    } else {
      if (!prevSelected.current) return;
      unselectProduct(productId);
    }
  }, [selected, productId, selectProduct, unselectProduct]);

  return (
    <div>
      <ImageWrapper>
        <img src={imageUrl} alt={name} />
        {promotionBadge ? (
          <div className="promotion">{promotionBadge}</div>
        ) : null}
        <div
          className={`checkboxWrap ${selected && 'active'} `}
          onClick={() => setSelected(!selected)}
        >
          {selected ? <div className="checkbox">&#10004;</div> : null}
        </div>
      </ImageWrapper>
      <DescriptionWrapper>
        <div className="priceWrap">
          <div className="productName">{name}</div>
          <div>
            <span className="price">{price}</span>
            <span className="priceWas">{priceWas}</span>
          </div>
          <div className="quantityWrap">
            {quantity ? (
              <div className="quantity">{quantity} in stock</div>
            ) : (
              <div className="out">OUT OF STOCK</div>
            )}
            {quantity && lowOnStock ? (
              <div className="low">LOW ON STOCK</div>
            ) : null}
          </div>
        </div>
      </DescriptionWrapper>
    </div>
  );
};

const ImageWrapper = styled.div`
  position: relative;

  .promotion {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(255, 0, 0, 0.4);
    text-align: center;
    color: #fff;
    font-weight: 500;
    padding: 5px;
  }

  .checkboxWrap {
    position: absolute;
    top: 20px;
    left: 20px;
    border-radius: 5px;
    width: 20px;
    height: 20px;
    background: #d9ccd9;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 3px;

    &.active {
      background-color: var(--purple);
    }

    .checkbox {
      color: #fff;
    }
  }
`;

const DescriptionWrapper = styled.div`
  .priceWrap {
    background-color: #fff;
    padding: 20px;
    min-height: 95px;

    .productName {
      margin-bottom: 5px;
    }

    .quantity {
      color: var(--green);
    }

    .out {
      color: var(--red);
    }

    .low {
      color: var(--orange);
    }

    .price {
      font-size: 16px;
      font-weight: 700;
    }

    .priceWas {
      font-size: 16px;
      font-weight: 500;
      color: var(--red);
      text-decoration: line-through;
      margin-left: 5px;
    }

    .quantityWrap {
      margin-top: 5px;
    }
  }
`;
