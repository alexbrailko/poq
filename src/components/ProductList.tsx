import { useEffect, FC } from 'react';
import { useProductsStore } from '../store/products';
import { ProductCard } from './ProductCard';
import styled from 'styled-components';

export const ProductList: FC = () => {
  const fetchProducts = useProductsStore((state) => state.fetchProducts);
  const products = useProductsStore((state) => state.products);
  const loading = useProductsStore((state) => state.loading);
  const hasErrors = useProductsStore((state) => state.hasErrors);

  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  const displayProducts = () => {
    return products.map((p) => <ProductCard key={p.productId} {...p} />);
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (hasErrors) {
    return <div>Error loading products</div>;
  }

  return <List>{displayProducts()}</List>;
};

const List = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-column-gap: 20px;
  grid-row-gap: 20px;
`;
